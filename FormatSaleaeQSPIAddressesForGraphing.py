#!/usr/bin/python3

import sys
import csv
import json
import operator

# Usage:  python3 FormatQSPIAddressesForGraphing.py [hex,dec] saleae_input.csv > processed_output.csv
# e.g.    python3 FormatQSPIAddressesForGraphing.py dec saleae_input.csv > processed_output_dec.csv
# e.g.    python3 FormatQSPIAddressesForGraphing.py hex saleae_input.csv > processed_output_hex.csv

# Code assumes it's being passed Saleae-QSPI-analyzer-exported data like
# "QSPI","Command: 0x03 Read"
# "QSPI","Address: 0x10"
# ...
# "QSPI","Command: 0x0B Fast Read"
# "QSPI","Address: 0x40"
# ...
# "QSPI","Command: 0xBB Dual I/O Fast Read"
# "QSPI","Address: 0x1000"
# ...
# "Command: 0xEB Quad I/O Fast Read"
# "QSPI","Address: 0x100000"
# Only the above read commands are supported currently

with open(sys.argv[2]) as csvfile:
    data_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    previous_line_valid_command = 0
    i = int(0)
    print("point,value")
    for row in data_reader:
        # If the previous line was a valid read command, check if this line is an address. And if so, output it
        if(previous_line_valid_command):
            compound_address = row[1]
            toks = compound_address.split(": ")
            if(toks[0] != "Address"):
                previous_line_valid_command = 0
                continue
            address = toks[1]
            if(sys.argv[1] == "hex"):
                print("%d,%s" % (i, address)) #Natural hex output
            else:
                print("%d,%d" % (i, int(address, 16))) #hex converted to decimal for DatPlot which doesn't understand hex
            previous_line_valid_command = 0
            i+=1
        else:
            # Check if this line is a valid command
            if (
               row[1] == "Command: 0x03 Read" or
               row[1] == "Command: 0x0B Fast Read" or
               row[1] == "Command: 0xBB Dual I/O Fast Read" or
               row[1] == "Command: 0xEB Quad I/O Fast Read"
               ):
                previous_line_valid_command = 1
